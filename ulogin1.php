<!DOCTYPE html>   
<html>   
<head>  
<meta name="viewport" content="width=device-width, initial-scale=1">  
<title> Login Page </title>  
<style>   
Body {  
  font-family: Calibri, Helvetica, sans-serif, serif;  
  background: url(paper.jpg);
  background-repeat:no-repeat;
  background-size:cover;
  
}  
button {   
       background-color: #00938C;   
       width: 50%;  
       color: #002493  ; 
	   font-family:Georgia;
       font-size: 16px;	   
       padding: 15px;   
       margin: 10px 0px;   
       border: none;   
       cursor: pointer;   
	   border-radius: 15px;
	   box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
         }  

  
    }   
 lable{
      width: 50px;
      display: inline-block;
      }
 input[type=text], input[type=password] {   
        width: 50%;   
        margin: 8px 0;  
        padding: 12px 20px;   
        display: inline-block;   
        border: 2px solid Teal;   
        box-sizing: border-box;   
    }  
 button:hover {   
        opacity: 10;   
        background-color: #0A6606;
        color: white;
    }   

 .container {   
        
        border-radius: 15px;
        width: 50%;
		top: 15%;
		left:25%;
        padding: 75px; 
        position:absolute;		
        color: #000000;
		transform: translate (-50%, -50%);
		box-shadow: 8px 8px 50px #000000;
		box-sizing: border-box;
    
    }   
	h1{
		font-family:Georgia;
		font-size: 35px;
		color:#A52A2A;
	}
</style>   
</head>    
<body>     
    <form align="center" method="POST" action="login_action.php">  
        <div class="container">   
		 <center> <h1>  Login Here </h1> </center>  
            <label>Username : </label>   
            <input type="text" placeholder="Enter Username" name="username" required>  </br> </br>
            <label>Password : </label>   
            <input type="password" placeholder="Enter Password" name="password" required>  </br> </br>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            <button type="submit"> Login </button></br> </br>
			 
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            Forgot <a href="#"> password? </a>   
        </div>  
   
     
    </form>     
</body>     
</html>  
